import 'package:apl_project/common_widgets/container.dart';
import 'package:apl_project/screens/action_screen/action_hub_screen_opn.dart';
import 'package:apl_project/utils/image_res.dart';
import 'package:apl_project/utils/string_res.dart';
import 'package:flutter/material.dart';

import '../../utils/icon_res.dart';

// ignore: must_be_immutable
class ActionScreen extends StatefulWidget {
  ActionScreen({super.key});

  List<Widget> widgetOptions = <Widget>[
    const Text('Home Page',
        style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),
    const Text('Search Page',
        style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),
    const Text('Profile Page',
        style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),
  ];

  @override
  State<ActionScreen> createState() => _ActionScreenState();
}

class _ActionScreenState extends State<ActionScreen> {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    int selectedIndex = 0;
    void onItemTapped(int index) {
      setState(() {
        selectedIndex = index;
      });
    }
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: Padding(
            padding: EdgeInsets.only(top: height * 0.02, left: width * 0.05),
            child: Image.asset(ImageRes.accountCircle),
          ),
          centerTitle: true,
          title: Padding(
            padding: EdgeInsets.only(
              top: height * 0.02,
            ),
            child: const Text(
              StringRes.apl,
              style: TextStyle(fontFamily: "aplFonts", fontSize: 30),
            ),
          ),
          actions: [
            Padding(
              padding: EdgeInsets.only(
                top: height * 0.02,
                left: width * 0.02,
              ),
              child: Image.asset(ImageRes.notificationBell),
            ),
            Padding(
              padding: EdgeInsets.only(
                  top: height * 0.02, left: width * 0.04, right: width * 0.05),
              child: Image.asset(ImageRes.walletIcon),
            ),
          ],
          bottom: const PreferredSize(
              preferredSize: Size(0, 10), child: SizedBox()),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.only(
                  top: height * 0.0130,
                ),
                child: Stack(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                          top: height * 0.305, left: width * 0.047),
                      child: commonContainer(context),
                    ),
                    Positioned(
                      left: width * 0.06,
                      top: height * 0.250,
                      child: SizedBox(
                        width: width * 0.3,
                        height: height * 0.185,
                        child: const Image(
                          image: AssetImage(
                            ImageRes.playerImage,
                          ),
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                    Row(
                      children: [
                        SizedBox(width: width * 0.05),
                        IconRes.dot,
                        SizedBox(width: width * 0.03),
                        const Text(
                          StringRes.rcb,
                          style:
                              TextStyle(fontFamily: 'aplFonts', fontSize: 28.5),
                        )
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                        top: height * 0.001,
                        left: width * 0.365,
                      ),
                      child: SizedBox(
                        height: height * 0.150,
                        width: width * 0.290,
                        child: Image.asset(ImageRes.personImage),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                        top: height * 0.1290,
                        left: width * 0.080,
                      ),
                      child: container(context),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          top: height * 0.240, left: width * 0.615),
                      child: Container(
                        width: 130,
                        height: 45,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: const Color(0xff007fff),
                        ),
                        child: Stack(
                          children: [
                            Row(
                              children: [
                                SizedBox(width: width * 0.004),
                                Padding(
                                  padding: EdgeInsets.only(
                                    top: height * 0.00210,
                                  ),
                                  child: Container(
                                    width: width*0.130,
                                    height: height*0.0550,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(8),
                                        color: Colors.white),
                                    child: Image.asset(ImageRes.emptyWallet,
                                        scale: 0.90),
                                  ),
                                ),
                                SizedBox(width: width * 0.02),
                                RichText(
                                  text: const TextSpan(
                                    children: [
                                      TextSpan(
                                          text: StringRes.text90,
                                          style: TextStyle(
                                              fontSize: 22,
                                              fontFamily: 'aplFonts')),
                                      TextSpan(
                                          text: StringRes.cr,
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600))
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(
                        left: width * 0.0210, top: height * 0.0170),
                    child: Row(
                      children: [
                        commonContainer2(
                            105,
                            30,
                            [const Color(0xff0059b4), const Color(0xff65b2ff)],
                            StringRes.playerList,
                            14,
                            left: 2,
                            top: 2),
                        const SizedBox(
                          width: 2,
                        ),
                        commonContainer2(
                            60,
                            30,
                            [const Color(0xff0059b4), const Color(0xff65b2ff)],
                            StringRes.price,
                            14,
                            left: 2,
                            top: 2),
                        const SizedBox(
                          width: 2,
                        ),
                        commonContainer2(
                            105,
                            30,
                            [const Color(0xff0059b4), const Color(0xff65b2ff)],
                            StringRes.playerList,
                            14,
                            left: 2,
                            top: 2),
                        const SizedBox(
                          width: 2,
                        ),
                        commonContainer2(
                            70,
                            30,
                            [const Color(0xff0059b4), const Color(0xff65b2ff)],
                            StringRes.price,
                            14,
                            left: 2,
                            top: 2),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        left: width * 0.0210, top: width * 0.0250),
                    child: Row(
                      children: [
                        commonContainer2(
                            105,
                            40,
                            [const Color(0xff0059b4), const Color(0xff007fff)],
                            StringRes.shikharDhaWan,
                            12,
                            left: 20,
                            top: 2),
                        const SizedBox(
                          width: 2,
                        ),
                        commonContainer2(
                            60,
                            40,
                            [const Color(0xff0059b4), const Color(0xff007fff)],
                            StringRes.text1by2l,
                            13,
                            left: 7,
                            top: 2),
                        const SizedBox(
                          width: 2,
                        ),
                        commonContainer2(
                            105,
                            40,
                            [const Color(0xff0059b4), const Color(0xff007fff)],
                            StringRes.shikharDhaWan,
                            12,
                            left: 20,
                            top: 2),
                        const SizedBox(
                          width: 2,
                        ),
                        commonContainer2(
                            70,
                            40,
                            [const Color(0xff0059b4), const Color(0xff007fff)],
                            StringRes.text1by2l,
                            13,
                            left: 7,
                            top: 2),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        left: width * 0.0210, top: width * 0.01),
                    child: Row(
                      children: [
                        commonContainer2(
                            105,
                            40,
                            [const Color(0xff0059b4), const Color(0xff007fff)],
                            StringRes.sachinTenDulKar,
                            12,
                            left: 20,
                            top: 2),
                        const SizedBox(
                          width: 2,
                        ),
                        commonContainer2(
                            60,
                            40,
                            [const Color(0xff0059b4), const Color(0xff007fff)],
                            StringRes.text1by2cr,
                            13,
                            left: 7,
                            top: 2),
                        const SizedBox(
                          width: 2,
                        ),
                        commonContainer2(
                            105,
                            40,
                            [const Color(0xff0059b4), const Color(0xff007fff)],
                            StringRes.sachinTenDulKar,
                            12,
                            left: 20,
                            top: 2),
                        const SizedBox(
                          width: 2,
                        ),
                        commonContainer2(
                            70,
                            40,
                            [const Color(0xff0059b4), const Color(0xff007fff)],
                            StringRes.text1by2cr,
                            13,
                            left: 7,
                            top: 2),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        left: width * 0.0210, top: width * 0.01),
                    child: Row(
                      children: [
                        commonContainer2(
                            105,
                            40,
                            [const Color(0xff0059b4), const Color(0xff007fff)],
                            StringRes.mahendraSinghDhoni,
                            11,
                            left: 2,
                            top: 2),
                        const SizedBox(
                          width: 2,
                        ),
                        commonContainer2(
                            60,
                            40,
                            [const Color(0xff0059b4), const Color(0xff007fff)],
                            StringRes.text12cr,
                            13,
                            left: 7,
                            top: 2),
                        const SizedBox(
                          width: 2,
                        ),
                        commonContainer2(
                            105,
                            40,
                            [const Color(0xff0059b4), const Color(0xff007fff)],
                            StringRes.mahendraSinghDhoni,
                            11,
                            left: 2,
                            top: 2),
                        const SizedBox(
                          width: 2,
                        ),
                        commonContainer2(
                            70,
                            40,
                            [const Color(0xff0059b4), const Color(0xff007fff)],
                            StringRes.text12cr,
                            13,
                            left: 7,
                            top: 2),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        left: width * 0.0210, top: width * 0.01),
                    child: Row(
                      children: [
                        commonContainer2(
                            105,
                            40,
                            [const Color(0xff0059b4), const Color(0xff007fff)],
                            StringRes.shikharDhaWan,
                            12,
                            left: 20,
                            top: 2),
                        const SizedBox(
                          width: 2,
                        ),
                        commonContainer2(
                            60,
                            40,
                            [const Color(0xff0059b4), const Color(0xff007fff)],
                            StringRes.text02cr,
                            13,
                            left: 8,
                            top: 2),
                        const SizedBox(
                          width: 2,
                        ),
                        commonContainer2(
                            105,
                            40,
                            [const Color(0xff0059b4), const Color(0xff007fff)],
                            StringRes.shikharDhaWan,
                            12,
                            left: 20,
                            top: 2),
                        const SizedBox(
                          width: 2,
                        ),
                        commonContainer2(
                            70,
                            40,
                            [const Color(0xff0059b4), const Color(0xff007fff)],
                            StringRes.text02cr,
                            13,
                            left: 8,
                            top: 2),
                      ],
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Padding(
                    padding: EdgeInsets.only(
                        left: width * 0.0520, top: height * 0.0130),
                    child: InkWell(
                      onTap: () {
                         Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const ActionHubScreen()));
                      },
                      child: Container(
                          height: height * 0.0450,
                          width: width * 0.350,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(6),
                              border:
                                  Border.all(color: Colors.blue, width: 1.4)),
                          child: const Center(
                              child: Text(
                            StringRes.actionHub,
                            style: TextStyle(
                                fontFamily: 'aplFonts',
                                color: Color(0xff007fff)),
                          ))),
                    ),
                  ),
                  SizedBox(width: width * 0.185),
                  Padding(
                    padding: EdgeInsets.only(top: height * 0.0120),
                    child: Container(
                      width: width * 0.350,
                      height: height * 0.0600,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: const Color(0xff007fff)),
                      child: const Center(
                        child: Text('+BID',
                            style: TextStyle(
                              fontFamily: 'aplFonts',
                              color: Colors.white,
                              fontSize: 22,
                            )),
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(width: width * 0.05),
              Padding(
                padding: EdgeInsets.only(
                  top: height * 0.0150,
                ),
                child: const Divider(
                    color: Colors.black,
                    indent: 40,
                    endIndent: 40,
                    thickness: 1.050),
              ),
              Center(
                child: RichText(
                  text: TextSpan(
                    children: [
                      const TextSpan(
                        text: StringRes.currentBidBy,
                        style: TextStyle(
                            color: Color(0xff007fff),
                            fontFamily: 'aplFonts',
                            fontSize: 16),
                      ),
                      TextSpan(
                          text: StringRes.mi,
                          style: TextStyle(
                              color: Colors.red.shade400,
                              fontFamily: 'aplFonts',
                              fontSize: 16))
                    ],
                  ),
                ),
              ),
              const Divider(
                  color: Colors.black,
                  indent: 40,
                  endIndent: 40,
                  thickness: 1.050),
            ],
          ),
        ),
        bottomNavigationBar: BottomNavigationBar(
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                  icon: SizedBox(
                    height: 40,
                    width: 40,
                    child: Image(
                      height: 40,
                      width: 40,
                      image: AssetImage(
                        ImageRes.home,
                      ),
                    ),
                  ),
                  label: 'Home',
                  backgroundColor: Colors.black),
              BottomNavigationBarItem(
                  icon: SizedBox(
                      height: 40,
                      width: 40,
                      child: Image(image: AssetImage(ImageRes.reward))),
                  label: 'Search',
                  backgroundColor: Colors.yellow),
              BottomNavigationBarItem(
                icon: SizedBox(
                    height: 40,
                    width: 40,
                    child: Image(image: AssetImage(ImageRes.myMatches))),
                label: 'Profile',
                backgroundColor: Colors.blue,
              ),
              BottomNavigationBarItem(
                icon: SizedBox(
                    height: 40,
                    width: 40,
                    child: Image(image: AssetImage(ImageRes.chat))),
                label: 'Profile',
                backgroundColor: Colors.blue,
              ),
              BottomNavigationBarItem(
                icon: SizedBox(
                    height: 40,
                    width: 40,
                    child: Image(image: AssetImage(ImageRes.winner))),
                label: 'Profile',
                backgroundColor: Colors.blue,
              ),
            ],
            type: BottomNavigationBarType.shifting,
            currentIndex: selectedIndex,
            selectedItemColor: Colors.white,
            iconSize: 25,
            onTap: onItemTapped,
            elevation: 5),
      ),
    );
  }
}
