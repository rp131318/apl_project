import 'package:apl_project/screens/group_screen/view_edit_screen.dart';
import 'package:flutter/material.dart';

main() {
  runApp(
     MaterialApp(
      home: ViewEditScreen(),
      debugShowCheckedModeBanner: false,
    ),
  );
}
