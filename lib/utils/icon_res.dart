import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class IconRes{
  static const person = FaIcon(Icons.account_circle_outlined,size: 40,);
  static const notification = FaIcon(Icons.notifications,size: 40);
  static const wallet = FaIcon(Icons.radio,size: 40);
  static const dot = FaIcon(Icons.circle,size: 21.5,color: Colors.red,);
}