class ImageRes{
  static const accountCircle = 'assets/images/iconamoon_profile-circle-fill.png';
  static const notificationBell = 'assets/images/mingcute_notification-fill@2x.png';
  static const walletIcon = 'assets/images/empty-wallet.png';
  static const personImage = 'assets/images/Untitled-1 2@2x.png';
  static const lion = 'assets/images/path56.png';
  static const emptyWallet = 'assets/images/empty-wallet (1).png';
  static const playerImage = 'assets/images/image 28.png';
////----------------------------------------------------------------------bottomNavigationbar------------------------------------------------------------------------///
  static const home = 'assets/images/ic_round-home.png';
  static const reward = 'assets/images/Main Navigation Icons (1).png';
  static const myMatches = 'assets/images/Main Navigation Icons.png';
  static const chat = 'assets/images/Vector.png';
  static const winner = 'assets/images/fa6-solid_medal.png';
  static const backGround = 'assets/images/apl.JPEG';
  static const group = 'assets/images/Group 10419.png';
  static const closeBack = 'assets/images/ic_sharp-close.png';
  static const circleAvtar = 'assets/images/FC_Leicester_City_Logo 2.png';
  static const circleAvtar2 = 'assets/images/Ellipse 441.png';
}