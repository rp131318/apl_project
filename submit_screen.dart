import 'package:apl_project/common_widgets/container.dart';
import 'package:apl_project/utils/image_res.dart';
import 'package:apl_project/utils/string_res.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class SubmitScreen extends StatefulWidget {
  const SubmitScreen({super.key});
  @override
  State<SubmitScreen> createState() => _SubmitScreenState();
}

class _SubmitScreenState extends State<SubmitScreen> {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    int selectedIndex = 0;

    List<Widget> widgetOptions = <Widget>[
      const Text(
        'Home Page',
        style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold),
      ),
      const Text(
        'Search Page',
        style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold),
      ),
      const Text(
        'Profile Page',
        style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold),
      ),
    ];



    void onItemTapped(int index) {
      setState(
        () {
          selectedIndex = index;
        },
      );
    }

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: Padding(
            padding: EdgeInsets.only(top: height * 0.02, left: width * 0.05),
            child: Image.asset(ImageRes.accountCircle),
          ),
          centerTitle: true,
          title: Padding(
            padding: EdgeInsets.only(
              top: height * 0.02,
            ),
            child: const Text(
              StringRes.apl,
              style: TextStyle(fontFamily: "aplFonts", fontSize: 30),
            ),
          ),
          actions: [
            Padding(
              padding: EdgeInsets.only(
                top: height * 0.02,
                left: width * 0.02,
              ),
              child: Image.asset(ImageRes.notificationBell),
            ),
            Padding(
              padding: EdgeInsets.only(
                  top: height * 0.02, left: width * 0.04, right: width * 0.05),
              child: InkWell(child: Image.asset(ImageRes.walletIcon)),
            ),
          ],
          bottom: const PreferredSize(
              preferredSize: Size(0, 10), child: SizedBox()),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Stack(
                children: [
                  Padding(
                    padding:
                        EdgeInsets.only(top: height * .0250, left: width*0.12),
                    child: InkWell(
                     onTap: () {

                     },
                      child: Container(
                        width: width * 0.29,
                        height: height * 0.045,
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.blue, width: 1.5),
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: const Center(
                          child: Text(
                            StringRes.submitPlayer,
                            style: TextStyle(
                              fontSize: 11,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: width*0.04,top: height*0.125),
                    child: submitPlayerCommon(context,Colors.yellowAccent.shade400,Colors.black,StringRes.allRounderBatter,9),
                  ), Padding(
                    padding: EdgeInsets.only(left: width*0.04,top: height*0.335),
                    child: submitPlayerCommon(context,Colors.greenAccent.shade700,Colors.black,StringRes.bowler,10),
                  ), Padding(
                    padding: EdgeInsets.only(left: width*0.04,top: height*0.545),
                    child: submitPlayerCommon(context,Colors.white,Colors.blue,StringRes.wicketKeeperBatter,9),
                  ),
                   Padding(
                    padding:  EdgeInsets.only(top: height*0.094,left: width*0.015),
                    child: Image.asset(ImageRes.playerImage,scale: 0.715),
                  ), Padding(
                    padding:  EdgeInsets.only(top: height*0.3045,left: width*0.015),
                    child: Image.asset(ImageRes.playerImage,scale: 0.715),
                  ), Padding(
                    padding:  EdgeInsets.only(top: height*0.5145,left: width*0.015),
                    child: Image.asset(ImageRes.playerImage,scale: 0.715),
                  ),
                ],
              ),
              const SizedBox(height: 15),
              Padding(
                padding: const EdgeInsets.only(left: 20),
                child: Container(
                  height: height*0.06,
                  width: width*0.7,
                  decoration: BoxDecoration(color:const Color(0xff007fff),borderRadius: BorderRadius.circular(20)),
                  child: ElevatedButton(onPressed: () {

                  }, child: const Text(StringRes.submit)),
                ),
              )
            ],
          ),
        ),
        bottomNavigationBar: BottomNavigationBar(
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                  icon: SizedBox(
                    height: 40,
                    width: 40,
                    child: Image(
                      height: 40,
                      width: 40,
                      image: AssetImage(
                        ImageRes.home,
                      ),
                    ),
                  ),
                  label: 'Home',
                  backgroundColor: Colors.black),
              BottomNavigationBarItem(
                  icon: SizedBox(
                    height: 40,
                    width: 40,
                    child: Image(
                      image: AssetImage(ImageRes.reward),
                    ),
                  ),
                  label: 'Search',
                  backgroundColor: Colors.yellow),
              BottomNavigationBarItem(
                icon: SizedBox(
                  height: 40,
                  width: 40,
                  child: Image(
                    image: AssetImage(ImageRes.myMatches),
                  ),
                ),
                label: 'Profile',
                backgroundColor: Colors.blue,
              ),
              BottomNavigationBarItem(
                icon: SizedBox(
                    height: 40,
                    width: 40,
                    child: Image(image: AssetImage(ImageRes.chat))),
                label: 'Profile',
                backgroundColor: Colors.blue,
              ),
              BottomNavigationBarItem(
                icon: SizedBox(
                    height: 40,
                    width: 40,
                    child: Image(image: AssetImage(ImageRes.winner))),
                label: 'Profile',
                backgroundColor: Colors.blue,
              ),
            ],
            type: BottomNavigationBarType.shifting,
            currentIndex: selectedIndex,
            selectedItemColor: Colors.white,

            iconSize: 25,
            onTap: onItemTapped,
            elevation: 5),
      ),
    );
  }
}
