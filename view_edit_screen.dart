import 'dart:io';

import 'package:apl_project/common_widgets/container.dart';
import 'package:apl_project/utils/image_res.dart';
import 'package:apl_project/utils/string_res.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

// ignore: must_be_immutable
class ViewEditScreen extends StatefulWidget {
  ViewEditScreen({super.key});

  List<Widget> widgetOptions = <Widget>[
    const Text('Home Page',
        style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),
    const Text('Search Page',
        style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),
    const Text('Profile Page',
        style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),
  ];

  @override
  State<ViewEditScreen> createState() => _ViewEditScreenState();
}

class _ViewEditScreenState extends State<ViewEditScreen> {
  int selectedIndex = 0;
  File? image;

  void onItemTapped(int index) {
    setState(
      () {
        selectedIndex = index;
      },
    );
  }

  void imagePicker() async {
    ImagePicker picker = ImagePicker();
    XFile? images = await picker.pickImage(source: ImageSource.gallery);
    // XFile? img = await picker.pickImage(source: ImageSource.camera);
    image = File(images!.path);
    setState(
      () {},
    );
    // image = File(img!.path);
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: Padding(
            padding: EdgeInsets.only(top: height * 0.02, left: width * 0.05),
            child: Image.asset(ImageRes.accountCircle),
          ),
          centerTitle: true,
          title: Padding(
            padding: EdgeInsets.only(
              top: height * 0.02,
            ),
            child: const Text(
              StringRes.apl,
              style: TextStyle(fontFamily: "aplFonts", fontSize: 30),
            ),
          ),
          actions: [
            Padding(
              padding: EdgeInsets.only(
                top: height * 0.02,
                left: width * 0.02,
              ),
              child: Image.asset(ImageRes.notificationBell),
            ),
            Padding(
              padding: EdgeInsets.only(
                  top: height * 0.02, left: width * 0.04, right: width * 0.05),
              child: Image.asset(ImageRes.walletIcon),
            ),
          ],
          bottom: const PreferredSize(
              preferredSize: Size(0, 10), child: SizedBox()),
        ),
        body: Column(
          children: [
            Stack(
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: width * 0.065, vertical: height * 0.019),
                  child: const Text(StringRes.viewEditNewGroup,
                      style: TextStyle(
                        fontFamily: 'aplFonts',
                        color: Color(0xff0059b4),
                      )),
                ),
                Padding(
                  padding: EdgeInsets.only(top: height * 0.06),
                  child: Divider(
                    thickness: 2,
                    color: Colors.grey.shade600,
                    endIndent: 25,
                    indent: 25,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: width * 0.048, vertical: height * 0.161),
                  child: editScreenCommonContainer(height * 0.485, width * 0.9),
                ),
                Padding(
                  padding:
                      EdgeInsets.only(top: height * 0.09, left: width * 0.325),
                  child: InkWell(
                    onTap: imagePicker,
                    child: CircleAvatar(
                      radius: height * 0.082,
                      child: image != null
                          ? ClipRRect(
                              borderRadius: BorderRadius.circular(50),
                              child: CircleAvatar(
                                radius: height * 0.082,
                                backgroundImage: FileImage(image!),
                              ),
                            )
                          : Image.asset(ImageRes.circleAvtar, scale: 0.5),
                    ),
                  ),
                ),
                Padding(
                  padding:
                      EdgeInsets.only(left: width * 0.55, top: height * 0.212),
                  child: Container(
                    height: 40,
                    width: 40,
                    decoration: BoxDecoration(
                        color: Colors.black54,
                        borderRadius: BorderRadius.circular(30)),
                    child: Image.asset(ImageRes.camera, scale: 0.9),
                  ),
                ),
                Padding(
                  padding:
                      EdgeInsets.only(left: width * 0.14, top: height * 0.7),
                  child: Container(
                    width: width * 0.7,
                    height: height * 0.055,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: const Color(0xff007fff),
                    ),
                    child: const Center(
                      child: Text(
                        StringRes.sendLink,
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w700,
                            color: Colors.white),
                      ),
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
        bottomNavigationBar: BottomNavigationBar(
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                  icon: SizedBox(
                    height: 40,
                    width: 40,
                    child: Image(
                      height: 40,
                      width: 40,
                      image: AssetImage(
                        ImageRes.home,
                      ),
                    ),
                  ),
                  label: 'Home',
                  backgroundColor: Colors.black),
              BottomNavigationBarItem(
                  icon: SizedBox(
                    height: 40,
                    width: 40,
                    child: Image(
                      image: AssetImage(ImageRes.reward),
                    ),
                  ),
                  label: 'Search',
                  backgroundColor: Colors.yellow),
              BottomNavigationBarItem(
                icon: SizedBox(
                  height: 40,
                  width: 40,
                  child: Image(
                    image: AssetImage(ImageRes.myMatches),
                  ),
                ),
                label: 'Profile',
                backgroundColor: Colors.blue,
              ),
              BottomNavigationBarItem(
                icon: SizedBox(
                    height: 40,
                    width: 40,
                    child: Image(image: AssetImage(ImageRes.chat))),
                label: 'Profile',
                backgroundColor: Colors.blue,
              ),
              BottomNavigationBarItem(
                icon: SizedBox(
                    height: 40,
                    width: 40,
                    child: Image(image: AssetImage(ImageRes.winner))),
                label: 'Profile',
                backgroundColor: Colors.blue,
              ),
            ],
            type: BottomNavigationBarType.shifting,
            currentIndex: selectedIndex,
            selectedItemColor: Colors.white,
            iconSize: 25,
            onTap: onItemTapped,
            elevation: 5),
      ),
    );
  }
}
